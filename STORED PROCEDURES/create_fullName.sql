CREATE DEFINER=`simplon`@`localhost` PROCEDURE `create_fullName`(
    OUT full_name VARCHAR (100))
BEGIN
	SELECT CONCAT(user.first_name, ' ', user.last_name) as full_name FROM user;
END