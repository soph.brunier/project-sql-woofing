CREATE DEFINER=`simplon`@`localhost` PROCEDURE `volunteers_possible_jobs`()
BEGIN
	SELECT CONCAT(u.first_name, ' ', u.last_name) as full_name,
	   GROUP_CONCAT(s.name) as skill_name,
       j.description as possible_jobs,
       CONCAT(uh.first_name, ' ', uh.last_name) as host_full_name,
       l.name as job_lodging,
       l.city as job_city
	FROM user u 
	INNER JOIN user_skills us ON u.id = us.id_user 
	JOIN skills s ON us.id_skill=s.idskills
	JOIN job_skills js ON js.id_skill=s.idskills
	JOIN job j ON j.id=js.id_job
    JOIN lodging l ON j.lodging_id=l.id
    JOIN user uh ON l.host_id=uh.id
	GROUP BY full_name, possible_jobs;
END