CREATE DEFINER=`simplon`@`localhost` PROCEDURE `lodging_jobs`()
BEGIN
	SELECT concat(u.first_name, ' ', u.last_name) as host_name,
			l.name as lodging_name, 
			l.city,
			j.description as job_description,
			j.start_date,
			j.end_date        
	FROM job j
	JOIN lodging l ON j.lodging_id=l.id
	JOIN user u ON l.host_id = u.id;
END