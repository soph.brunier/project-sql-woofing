CREATE DEFINER=`simplon`@`localhost` PROCEDURE `volunteers_skills`()
BEGIN

SELECT concat(u.first_name, ' ', u.last_name) as fullName, GROUP_CONCAT(s.name) as skills FROM user u
JOIN user_skills us
JOIN skills s WHERE u.id=us.id_user AND us.id_skill = s.idskills
GROUP BY fullName;

END