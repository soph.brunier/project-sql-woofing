CREATE DEFINER=`simplon`@`localhost` PROCEDURE `job_skills`()
BEGIN
	SELECT j.description as job_description, s.name as skill_name
	FROM job_skills js
	JOIN job j ON js.id_job=j.id
	JOIN skills s ON js.id_skill=s.idskills ;
END