CREATE DEFINER=`simplon`@`localhost` PROCEDURE `lodging_hosts`()
BEGIN
	SELECT concat(u.first_name,' ', u.last_name) as host_name,
		l.name, 
        concat(l.street_number, ' ', l.street_name) as adress,
        l.postal_code,
        l.city
	FROM lodging l 
    INNER JOIN user u WHERE l.host_id = u.id;

END