CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `simplon`@`localhost` 
    SQL SECURITY DEFINER
VIEW `volunteers_list_with_skills` AS
    SELECT 
        CONCAT(`u`.`first_name`, ' ', `u`.`last_name`) AS `fullName`,
        `u`.`email` AS `email`,
        `u`.`birth_date` AS `birth_date`,
        GROUP_CONCAT(`s`.`name`
            SEPARATOR ',') AS `skills`
    FROM
        ((`user` `u`
        JOIN `user_skills` `us`)
        JOIN `skills` `s`)
    WHERE
        ((`u`.`id` = `us`.`id_user`)
            AND (`us`.`id_skill` = `s`.`idskills`))
    GROUP BY CONCAT(`u`.`first_name`, ' ', `u`.`last_name`)