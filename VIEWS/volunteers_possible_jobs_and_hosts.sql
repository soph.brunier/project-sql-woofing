CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `simplon`@`localhost` 
    SQL SECURITY DEFINER
VIEW `volunteers_possible_jobs_and_hosts` AS
    SELECT 
        CONCAT(`u`.`first_name`, ' ', `u`.`last_name`) AS `full_name`,
        GROUP_CONCAT(`s`.`name`
            SEPARATOR ',') AS `skill_name`,
        `j`.`description` AS `possible_jobs`,
        CONCAT(`uh`.`first_name`, ' ', `uh`.`last_name`) AS `host_full_name`,
        `l`.`name` AS `job_lodging`,
        `l`.`city` AS `job_city`
    FROM
        ((((((`user` `u`
        JOIN `user_skills` `us` ON ((`u`.`id` = `us`.`id_user`)))
        JOIN `skills` `s` ON ((`us`.`id_skill` = `s`.`idskills`)))
        JOIN `job_skills` `js` ON ((`js`.`id_skill` = `s`.`idskills`)))
        JOIN `job` `j` ON ((`j`.`id` = `js`.`id_job`)))
        JOIN `lodging` `l` ON ((`j`.`lodging_id` = `l`.`id`)))
        JOIN `user` `uh` ON ((`l`.`host_id` = `uh`.`id`)))
    GROUP BY CONCAT(`u`.`first_name`, ' ', `u`.`last_name`) , `j`.`description`