CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `simplon`@`localhost` 
    SQL SECURITY DEFINER
VIEW `hosts_list_with_lodging` AS
    SELECT 
        CONCAT(`u`.`first_name`, ' ', `u`.`last_name`) AS `full_name`,
        `u`.`email` AS `email`,
        `l`.`name` AS `lodging`,
        `l`.`description` AS `description`,
        CONCAT(`l`.`street_number`,
                ' ',
                `l`.`street_name`) AS `adress`,
        `l`.`postal_code` AS `postal_code`,
        `l`.`city` AS `city`,
        `l`.`places` AS `places`
    FROM
        (`lodging` `l`
        LEFT JOIN `user` `u` ON ((`l`.`host_id` = `u`.`id`)))
    WHERE
        (`u`.`role` = 'host')