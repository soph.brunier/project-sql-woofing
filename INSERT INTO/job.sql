USE `project_sql_woofing`;
INSERT INTO `job` (lodging_id, description, start_date, end_date, volunteers_needed) 
VALUES (1, 'job à la Chaumière - Boulangerie, Bricolage, Nettoyage', '2020-12-01','2020-12-08', 3);
INSERT INTO `job` (lodging_id, description, start_date, end_date, volunteers_needed) 
VALUES (2, 'Fruits, permaculture et legumes', '2021-03-15','2021-03-30', 2);
INSERT INTO `job` (lodging_id, description, start_date, end_date, volunteers_needed) 
VALUES (3, 'Permaculture 1', '2021-04-01', '2021-04-15', '4');
INSERT INTO `job` (lodging_id, description, start_date, end_date, volunteers_needed) 
VALUES (3, 'Permaculture 2', '2021-04-16', '2021-04-30', '4');

