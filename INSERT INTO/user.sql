USE `project_sql_woofing`;
INSERT INTO `user` (first_name, last_name, birth_date, email, password, role, creation_date) 
VALUES ('Emily','Chaumes', '1978-05-02', 'e.chaumes@gmail.com', 'Xdj3ft45l', 'host', now());
INSERT INTO `user` (first_name, last_name, birth_date, email,password, role, creation_date) 
VALUES ('Olivier','Genet', '1959-12-07', 'o.genet@laferme.com', 'JRe65lk4', 'host', now());
INSERT INTO `user` (first_name, last_name, birth_date, email, password, role, creation_date) 
VALUES ('Jonnhy','Begoude', '1976-03-09', 'JoBgood@gmail.com', 'F4gh0Klo8', 'volunteer', now());
INSERT INTO `user` (first_name, last_name, birth_date, email, password, role, creation_date) 
VALUES ('Coline','Vanhooten', '1983-10-25', 'cocoVH@hotmail.fr', 'hj208jiRT9', 'volunteer', now());
INSERT INTO `user` (first_name, last_name, birth_date, email, password, role, creation_date) 
VALUES ('Vincent','Tim', '1990-10-03', 'vincent_time@voila.fr', 'rJf58Jko003', 'volunteer', now());
INSERT INTO `user` (first_name, last_name, birth_date, email, password, role, creation_date) 
VALUES ('Paulo','Marquez', '1990-02-15', 'pmarquez@gmail.com', 'jkfds83092JJD', 'volunteer', now());
INSERT INTO `user` (first_name, last_name, birth_date, email, password, role, creation_date) 
VALUES ('Marie','Bernard', '1988-05-25', 'marieB@voila.fr', 'fjkdsmç_fds', 'volunteer', now());
INSERT INTO `user` (first_name, last_name, birth_date, email, password, role, creation_date) 
VALUES ('Jean Michel','Bernard', '1968-06-05', 'jmb@gmail.com', '12FR45ki6', 'host', now());
INSERT INTO `user` (first_name, last_name, birth_date, email, password, role, creation_date) 
VALUES ('Marco','Polo', '1967-12-23', 'navigate@gmail.com', 'REzKFO34Dfk34', 'host', now());
INSERT INTO `user` (first_name, last_name, birth_date, email, password, role, creation_date) 
VALUES ('Christophe','Peyre', '1980-12-01', 'cpeyre@hotmail.com', 'Hrk9o0kfds73', 'volunteer', now());
INSERT INTO `user` (first_name, last_name, birth_date, email, password, role, creation_date) 
VALUES ('Joanne','Livet', '1998-01-02', 'jlivet@hotmail.com', 'jie9j3LOp3M', 'volunteer', now());
