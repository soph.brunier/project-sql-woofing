USE `project_sql_woofing`;
INSERT INTO `lodging` (name, description, street_number, street_name, postal_code, city, host_id, places) 
VALUES ('La Chaumière','Petite ferme familiale dans l\'aveyron', 2, 'rue des champs', 12520, 'Verrières', 1, 3);
INSERT INTO `lodging` (name, description, street_number, street_name, postal_code, city, host_id, places) 
VALUES ('La Petite Ferme','Un coin de paradis', 695, 'Chemin des Vendanges', 84000, 'Avignon', 2, 4);
INSERT INTO `lodging` (name, description, street_number, street_name, postal_code, city, host_id, places) 
VALUES ('La Petite Maison Dans La Prairie','Projet de permaculture, éco-construction et construction avec matériaux de récupération', 23, 'Rue de la paix', 63000, 'Clermont-Ferrand', 8, 5);
INSERT INTO `lodging` (name, description, street_number, street_name, postal_code, city, host_id, places) 
VALUES ('Les Jardins du Rhone','La verdure en ville', 3, 'Avenue de la République', 69001, 'LYON', 9, 8);