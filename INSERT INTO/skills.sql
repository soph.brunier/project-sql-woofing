USE `project_sql_woofing`;
INSERT INTO `skills` (name, description) VALUES ('Apiculture', 'par amour des abeilles');
INSERT INTO `skills` (name, description) VALUES ('Fruiticulture', 'la cueillette des fruits');
INSERT INTO `skills` (name, description) VALUES ('Permaculture', 'la biodiversité des systèmes');
INSERT INTO `skills` (name, description) VALUES ('Maraîchage', 'cultiver pour soi');
INSERT INTO `skills` (name, description) VALUES ('Boulangerie', 'pain et autres délices');
INSERT INTO `skills` (name, description) VALUES ('Nettoyage', 'pour faire place nette');
INSERT INTO `skills` (name, description) VALUES ('Bricolage', 'pour les marteaux des clous');
